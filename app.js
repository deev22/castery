var fs = require('fs'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    http = require('http'),
    https = require('https'),
    log = require('./app/services/logger-service') ;

var config;

try {
    config = require('./config.js');
} catch (error) {
    console.error('Unable to find config', error)
}

http.globalAgent.maxSockets = 100;
https.globalAgent.maxSockets = 100;

var mongodbURI = 'mongodb://' + config.MONGO_ADDRESS + config.MONGO_DB_NAME;
var connectMongo = function () {
    mongoose.connect(mongodbURI)
};
connectMongo();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connectMongo);

var models_path = __dirname + '/app/models';
fs.readdirSync(models_path).forEach(function(file) {
    if (file.substring(-3) === '.js') {
        require(models_path + '/' + file)
    }
});

//require('./config/passport')(passport);

var app = require('./config/express')(passport, mongodbURI);
require('./config/routes')(app);
function portInfo() {
	log.info('Express HTTPS server listening on port ' + app.get('port'))
}
if (process.env.DEBUG === 'true') {
  var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8')
    , certificate = fs.readFileSync('sslcert/server.crt', 'utf8')
    , credentials = {key: privateKey, cert: certificate};
    var server = https.createServer(credentials, app);
    io = require('socket.io').listen(server.listen(app.get('port'), portInfo));
}
else {
    var server = http.createServer(app);
    io = require('socket.io').listen(server.listen(app.get('port'), portInfo));
}
namespaces = [];

exports = module.exports = app;
