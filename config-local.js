'use strict';

module.exports = {
    GCLOUD_PROJECT_ID: '<Your_gcloud_project_id>',
    JWT_SECRET: '<Choose_a_long_key>',
    JWT_EXPIRY: 2 * 60 * 5,
    JWT_NO_RENEWAL: 2 * 60 * 5 * 15,
    JWT_MAIL_EXPIRY: 15 * 60,
    JWT_MAIL_SECRET: '<Choose_a_long_key>',
    EXPRESS_PORT: 3000,
    MONGO_ADDRESS: "<Mongo_Instance_address>",
    MONGO_DB_NAME: "<Your_db_name>",
    MANDRILL_KEY: '<Your_Mandrill_Key>',
    PATH_TO_GCLOUD_KEY: "/path/to/credentials"
};
