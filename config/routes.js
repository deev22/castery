"use strict";

module.exports = function (app) {
  app.use('/auth', require('../app/api/auth'));
  app.use('/call', require('../app/api/call'));
  app.use('/sound', require('../app/api/sound'));
  app.use('/archive', require('../app/api/archive'));
  app.use('/log', require('../app/api/log'));
  app.use('/webhook', require('../app/api/webhook'));
  app.use('/configs', require('../app/api/config'));
};
