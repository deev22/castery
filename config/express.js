var express = require('express'),
    path = require('path'),
    config = require ('../config');

module.exports = function (passport) {
    var app = express(),
        root = path.normalize(__dirname + '/..');

    app.set('showStackError', true);
    app.set('port', process.env.PORT || config.EXPRESS_PORT);
    app.set('views', root + '/app/views');
    app.set('view engine', 'ejs');
    var bodyParser = require('body-parser');
    var methodOverride = require('method-override');
    app.use(function(req, res, next){
        if (req.is('text/*')) {
            req.text = '';
            req.setEncoding('utf8');
            req.on('data', function(chunk){ req.text += chunk });
            req.on('end', next);
        } else {
            next();
        }
    });
    app.use(bodyParser({limit: '50mb'}));
    app.use(methodOverride());

    // CORS
    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept, castery-resource-token");
        next();
    });
    app.use(passport.initialize());
    app.use(express.static(root + '/public'));
    return app;
};