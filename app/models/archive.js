"use strict";

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = mongoose.Schema.Types.ObjectId,
    Number = mongoose.Schema.Types.Number;

var ArchiveSchema = new Schema({
  creator: { type: Number, required: true },
  createdAt: { type: Date, required: true, 'default': Date.now },
  name: { type: String, required: true },
  localRecordings: { type: Array, required: true }
});

module.exports = mongoose.model('Archive', ArchiveSchema);
