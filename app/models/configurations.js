"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.Types.ObjectId,
    Number = mongoose.Schema.Types.Number;

var ConfigSchema = new Schema({
    chunkSizeCoefficient: { type: Number, required: true }
});

module.exports = mongoose.model('Configs', ConfigSchema);
