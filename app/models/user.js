var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uniqueValidator = require('mongoose-unique-validator'),
    scrypt = require('scrypt'),
    AutoIncrement = require('mongoose-sequence'),
    validator = require('validator');

var UserSchema = new Schema({
    _id: Number,
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: {  type: String,
        unique: true,
        required: true
    },
    verified: { type: Boolean, default: false },
    country: { type: String, required: true },
    hashed_password: { type: String, default: '' },
    user_image: String,
    soundcloud_id: String,
    facebook_id: String,
    gplus_id: String,
    twitter_id: String,
    createdAt: { type: Date, 'default': Date.now },
    user_settings: { type: Object,
        'default': {
            sounds_quota: 100,
            projects_quota: 20
        }
    }
}, { _id: false });

UserSchema.plugin(AutoIncrement);

/** Override toJSON **/

UserSchema.set('toJSON', {
    transform: function(doc, ret, options) {
        delete ret.hashed_password;
        delete ret.user_settings;
        return ret;
    }
});

UserSchema
    .virtual('password')
    .set(function(password) {
        if (password.length < 8) {
            this.invalidate('password', 'Password is too short')
        }
        this._password = password;
        this.hashed_password = this.encryptPassword(password)
    })
    .get(function() { return this._password });

/**
 * Pre-save hook
 */

UserSchema.pre('save', function(next) {
    if (!this.isNew) return next();

    var isSocial = (this.facebook_id || this.soundcloud_id);

    if (!isSocial) {
        if (!this.password) {
            return next(new Error('User present, password not present'))
        }
        if (!this.email) {
            return next(new Error('User present, email not present'))
        }
    }
    return next()
});

/**
 * Methods
 */

UserSchema.statics.findByEmail = function(email, cb) {
  this.findOne({email: email}, function(err, usr) {
      if(err || !usr) {
          cb(err, null)
      } else {
          cb(false, usr)
      }
  })
};


// TODO synchronous?
UserSchema.methods = {

    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     * @return {Boolean}
     * @api public
     */

    authenticate: function (plainText) {
        return this.encryptPassword(plainText) === this.hashed_password
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @return {String}
     * @api public
     */

    encryptPassword: function (password) {
        if (!password) return '';
        var scryptParameters = scrypt.params(0.1);
        var key = new Buffer(password);
        scrypt.hash.config.outputEncoding = "hex";
        return scrypt.hash(key, scryptParameters);
    }
};

/**
 * Validators
 */

UserSchema.plugin(uniqueValidator, { message: '{PATH}.NOT_UNIQUE' });

var User = mongoose.model('User', UserSchema);

User.schema.path('email').validate(function (value) {
    return validator.isEmail(value)
}, 'Email is invalid');

module.exports = mongoose.model('User', UserSchema);
