"use strict";

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Schema.Types.ObjectId,
    uniqueValidator = require('mongoose-unique-validator');

var SoundSchema = new Schema({
    name: { type: String, required: true, unique: true },
    creator: { type: Number, required: true },
    createdAt: { type: Date, required: true, 'default': Date.now }
});

SoundSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Sound', SoundSchema);