var request = require('request');

exports.requestICE = function(cb) {
    var data = {
        form: {
	        ident: "castery",
	        secret: "<your_xirsys_secret_unused>",
	        domain: "castery.com",
	        application: "<your-app>",
	        room: "default",
	        secure: 1
        }
    };
    var responseHandler = function (error, response, body){
	    if (!error && response.statusCode === 200) {
		    var parsedBody, iceConfig;
		    try {
			    parsedBody = JSON.parse(body)
		    }
		    catch (e) {
			    return cb("Cannot parse response", null)
		    }
		    iceConfig = parsedBody.d.iceServers;
		    cb(null, iceConfig);
	    }
	    else {
		    cb(error, res)
	    }
    };
    request.post('https://api.xirsys.com/getIceServers', data, responseHandler);
};
