/**
 * Created by janesconference on 26/11/2014.
 */
"use strict";

var responseLookup = {
    'VALIDATION_ERROR': 400,
    'INSUFFICIENT_PARAMETERS': 400,
    'INVALID_TYPE': 400,
    'CALL_NOT_FOUND': 404,
    'INVALID_TOKEN_ISSUER': 400,
    'INVALID_PARAMETERS': 400,
    'INVALID_ORIGIN': 400,
    'CALL_ID_NEEDED': 400,
    'ARCHIVE_ID_NEEDED': 400,
    'GUEST_ARRAY_NEEDED': 400,
    'ARCHIVE_NAME_NEEDED': 400,
    'OK': 200,
    'SEND_RESOURCE': 202,
    'INVALID_CONTENT_TYPE': 415,
    'INVALID_MAIL_TOKEN': 401,
    'USER_NOT_FOUND': 404,
    'USER_NOT_VERIFIED': 401,
    'ERROR_SAVING_USER': 500,
    'ERROR_SAVING_CALL': 500,
    'ERROR_GENERATING_URLS': 500,
    'ERROR_FINDING_ARCHIVE': 500,
    'ERROR_STORING_ARCHIVE': 500,
    'ERROR_REMOVING_ARCHIVE': 500,
    'ERROR_SAVING_SOUND': 500,
    'ERROR_FINDING_SOUND': 500,
    'ERROR_FINDING_USER': 500,
    'ERROR_FINDING_CALL': 500,
    'ERROR_START_RECORDING': 500,
    'ERROR_STOP_RECORDING': 500,
    'ARCHIVE_NOT_STARTED': 404,
    'ERROR_SOUND_NOT_UNIQUE': 409,
    'STORAGE_ERROR': 500,
    'INVALID_PASSWORD': 401,
    'NOT_AUTHENTICATED': 401,
    'GENERIC_ERROR': 500
};

exports.send = function (res, type, msg, data) {
    if (responseLookup[type]) {
        res.json (responseLookup[type], {
            type: type,
            msg: msg,
            data: data
        })
    }
    else {
        res.json (responseLookup['GENERIC_ERROR'], {
            type: type,
            msg: msg,
            data: data
        })
    }
};
