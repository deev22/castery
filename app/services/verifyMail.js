var config = require ('../../config.js'),
    sendgrid  = require('sendgrid')(config.SENDGRID_KEY),
    ejs = require('ejs');

exports.sendVerifyMail = function(email, firstName, lastName, mailToken) {
    var template_path = __dirname + '/../views/mail/verify-mail.ejs';
    var read = require('fs').readFileSync;
    var template_content = ejs.render(read(template_path, 'utf-8'), {mailToken: mailToken});
    var email = new sendgrid.Email({
        to: email,
        toname: firstName + ' ' + lastName,
        from: 'no-reply@castery.com',
        subject: 'Verify your account',
        headers: {"Reply-To": "no-reply@castery.com"}
    });
    email.setHtml(template_content);
    sendgrid.send(email);
};

exports.sendForgotMail = function(email, mailToken) {
    var template_path = __dirname + '/../views/mail/forgot-mail.ejs',
    read = require('fs').readFileSync,
    template_content = ejs.render(read(template_path, 'utf-8'), {mailToken: mailToken}),
    email = new sendgrid.Email({
        to: email,
        from: 'no-reply@castery.com',
        subject: 'Reset your password.',
        headers: {"Reply-To": "no-reply@castery.com"}
    });
    email.setHtml(template_content);
    sendgrid.send(email);
};

exports.debugMail = function(email, message){
    var emailObj = new sendgrid.Email({
        to: email,
        from: 'no-reply@castery.com',
        subject: 'Debug email from castery'
    });
	emailObj.setHtml(message);
    sendgrid.send(emailObj, function (err, sc) {
        console.log(err);
        console.log(sc);
    });
};
