'use strict';

var config = require ('../../config.js'),
    expressJwt = require('express-jwt'),
    jwt = require('jsonwebtoken'),
    secret = config.JWT_SECRET,
    expiryTime = config.JWT_EXPIRY,
    mailExpiryTime = config.JWT_MAIL_EXPIRY,
    mailSecret = config.JWT_MAIL_SECRET;

exports.jwtCheck = expressJwt({secret: secret, userProperty: 'auth'});
exports.jwtCheckNoCred = expressJwt({secret: secret, userProperty: 'auth', credentialsRequired: false});

exports.generateToken = function (sub) {
    return jwt.sign({ sub: sub }, secret, { expiresInMinutes: expiryTime });
};

exports.renewToken = function (sub, iat) {
    return jwt.sign({ sub: sub, originalIat: iat }, secret, { expiresInMinutes: expiryTime });
};

exports.generateVerifyMailToken = function (id) {
    return jwt.sign({ id: id, iss: "verifyMail"}, mailSecret, { expiresInMinutes: mailExpiryTime });
};

exports.checkVerifyMailToken = function (token, cb) {
    jwt.verify(token, mailSecret, function(err, decoded) {
        cb (err, decoded)
    })
};

exports.decodeAuthToken = function (req, cb) {
  var token, parts;

  console.log ('hey, Im decodeAuthToken', req.headers.authorization);
  if (!req.headers.authorization) {
    console.log('nothing!');
    console.log(new UnauthorizedError('credentials_bad_format', { message: 'No Authorization token' }));
    console.log('nothing 2!');
    return cb(new UnauthorizedError('credentials_bad_format', { message: 'No Authorization token' }))
  }

  parts = req.headers.authorization.split(' ');
  if (parts.length === 2) {
      var scheme = parts[0];
      var credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
          token = credentials;
      }
  } else {
      return cb(new UnauthorizedError('credentials_bad_format', { message: 'Format is Authorization: Bearer [token]' }))
  }

  console.log('decodeAuthToken returning from token', token, jwt.decode(token), cb);
  return cb( null, jwt.decode(token) )
};