var twilio = require('twilio'),
    configs = require('../../config');

exports.generateToken = function (clientName, is_host) {
    var capability = new twilio.Capability(configs.TWILIO_ACCOUNT_SID, configs.TWILIO_AUTH_TOKEN);
    capability.allowClientIncoming(clientName);
    capability.allowClientOutgoing(configs.TWILIO_APP_SID);
    return capability.generate();
};