"use strict";

var Slack = require('node-slack'),
    config = require ('../../config.js');

exports.sendMessage = function (text, chanel, username, attachments) {
    var slack = new Slack(config.HOOK_URL);

    slack.send({
        text: text,
        channel: chanel,
        username: username,
        attachments: attachments
    }, function(err, response) {
        if(err) {
	        console.log('Slack error occurred - ' + err);
	        return err;
        }
        return true;

    });
};
