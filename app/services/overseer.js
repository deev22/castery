var uuid = require('node-uuid'),
    channels = {};
const MAXUSERS = 4;

function send(channel, originator, msg) {
    var selectedChannel = channels[channel];
    for (var uid in selectedChannel.participants) {
        if (selectedChannel.participants.hasOwnProperty(uid) && uid !== originator) {
            selectedChannel.participants[uid].messageCallback(msg, originator)
        }
    }

    selectedChannel.messages.push(
        {
            originator: originator,
            message: msg
        }

    )
}

function addUserToChannel(channel, user, callback) {
    if (!channels[channel]) {
        channels[channel] = {
            participants: {},
            messages: []
        }
    }
    if (!channels[channel][user]) {
        channels[channel].participants[user] = {}
    }
    channels[channel].participants[user].messageCallback = callback
}

function checkUnique (channel, id) {
    if (!id) {
        return false;
    }
    if (!channels[channel]) {
        return true;
    }
    return !(id in channels[channel])
}

function generateUserID(channel) {
    var proposedId, count = 0;
    do {
        proposedId = uuid.v4();
        count +=1;
    }
    while (!checkUnique(channel, proposedId) && count < 200);
    return proposedId
}

exports.getChannelHandler = getChannelHandler;
exports.sendMessage = sendMessage;

function sendMessage(channel, user, msg) {
    send (channel, user, msg)
}

function sendMessageBacklog (channel, user, cb) {
    var selectedChannelMessages = channels[channel].messages;
    for (var index = 0; index <  selectedChannelMessages.length; index +=1) {
     if (selectedChannelMessages[index].originator !== user) {
            cb(selectedChannelMessages[index].message, selectedChannelMessages[index].originator);
        }
    }

}

function getChannelHandler(channelID, isOwner) {
    var owner = isOwner;
    var channelid = channelID;
    var userid = generateUserID();

    function channelHandler(msg) {
        channelHandler.send(msg);
        return channelHandler
    }

    channelHandler.getUserID = function () {
        return userid
    };

    channelHandler.onMessage = function (callBack) {
        addUserToChannel(channelid, userid, callBack);
        sendMessageBacklog(channelid, userid, callBack);
        return channelHandler;
    };
    channelHandler.send = function (msg) {
        send(channelid, userid, msg);
        return channelHandler;
    };
    channelHandler.disconnect = function () {
        return channelHandler;
    };
    return channelHandler;
}
