var gcloud,
    bucket,
    config = require('../../config');

if (config.PATH_TO_GCLOUD_KEY) {
    gcloud = require('gcloud')({
        keyFilename: config.PATH_TO_GCLOUD_KEY,
        projectId: config.GCLOUD_PROJECT_ID
    });
}
else {
    gcloud = require('gcloud')({
        projectId: config.GCLOUD_PROJECT_ID
    });
}

bucket = gcloud.storage().bucket('app.castery.com');

exports.gcloud = gcloud;
exports.bucket = bucket;
