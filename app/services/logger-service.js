var bunyan = require('bunyan');
var log = bunyan.createLogger({name: "castery-node"});

module.exports = log;