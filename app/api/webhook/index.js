'use strict';

var express = require('express'),
 mailchimp = require('./mailchimp.controller'),
 router = express.Router();

router.post('/mailchimp1488', mailchimp.hook);
router.get('/mailchimp1488', mailchimp.validator);

module.exports = router;