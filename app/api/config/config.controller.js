"use strict";

var rService = require ('../../services/responses'),
Configs = require('../../models/configurations');

exports.setConfigs = function (req, res) {
    Configs.findOne(function(err, conf){
        if(!conf) {
            var conf = new Configs({chunkSizeCoefficient:req.body.chunkSizeCoefficient});
            conf.save();
            return rService.send(res, "OK", "OK", conf.toJSON());
        } else {
            conf.chunkSizeCoefficient = req.body.chunkSizeCoefficient;
            conf.save();
            return rService.send(res, "OK", "OK", conf.toJSON());
        }
    });
};

exports.getConfigs = function (req, res) {
    Configs.findOne(function(err, conf){
        if(!conf) {
            conf = {}
        }
        return rService.send(res, "OK", "OK", conf.toJSON());
    });
};
