'use strict';

var express = require('express');
var controller = require('./config.controller');
var router = express.Router();

router.post('/set-configs', controller.setConfigs)
router.get('/set-configs', controller.setConfigs)

router.get('/get-configs', controller.getConfigs)

module.exports = router;