"use strict";

var passport = require('passport'),
    scrypt = require ("scrypt"),
    User = require('../../models/user'),
    jwtUtils = require ('../../services/jwt-utils'),
    rService = require ('../../services/responses'),
    mailService = require ('../../services/verifyMail'),
    config = require ('../../../config.js'),
    log = require('../../services/logger-service').child({module: 'auth.controller'}),
    slack = require ('../../services/slack');

/**
 * Utility function: validate password
 */

function validatePassword (hashed_password, password, cb) {
    if (hashed_password) {
        scrypt.verify.config.keyEncoding = "utf8";
        scrypt.verify.config.hashEncoding = "hex";
        scrypt.verify(hashed_password, password, function(err, result) {
            if (err) {
                return cb (err);
            }
            if (result) {
                return cb (undefined, result);
            }
        })
    }
}

/* Route handlers */

exports.fbCallback = function(req, res, next) {
    passport.authenticate('facebook', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) {
            return res.redirect('/auth/failure');
        }
        var token = jwtUtils.generateToken(user.id);
        return res.render('after-auth', { state: 'success', token: token });
    })(req, res, next);
};

exports.scCallback = function(req, res, next) {
    passport.authenticate('soundcloud', function(err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect('/auth/failure');
        }
        var token = generateToken(user.id);
        return res.render('after-auth', { state: 'success', token: token });
    })(req, res, next);
};

exports.renew = function (err, req, res, next) {
    var oIat;
    if (err.name === 'UnauthorizedError' && err.inner && err.inner.name === 'TokenExpiredError') {
        // We need to decode the token manually
        jwtUtils.decodeAuthToken (req, function (err, auth) {
            if (err) {
                return rService.send (res, "NOT_AUTHENTICATED", "Token could not be decoded");
            }
            // iats are in seconds, JWT_NO_RENEWAL in minutes, Date.now() in millis. We just need another variable in fortnights.
            if (auth.originalIat &&  auth.originalIat < Math.round(Date.now()/1000) - config.JWT_NO_RENEWAL * 60) {
                return rService.send (res, "NOT_AUTHENTICATED", "Token too old");
            }
            User.findOne({ '_id': auth.sub }, function (err, user) {
                if (err) {
                    return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user");
                }
                if (!user) {
                    return rService.send (res, "USER_NOT_FOUND", "User not found");
                } else {
                    oIat = auth.originalIat || auth.iat;
                    return rService.send (res, "OK", "OK", { token: jwtUtils.renewToken (auth.sub, oIat) });
                }
            });

        });
    }
    else {
        return rService.send (res, "NOT_AUTHENTICATED", "Token is not valid")
    }
};

exports.me = function(req, res) {
    User.findById(req.auth.sub, function (err, user) {
        if (err) {
            return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user")
        }
        if (!user) {
            return rService.send (res, "USER_NOT_FOUND", "User not found")
        } else {
            return rService.send (res, "OK", "OK", user.toJSON())
        }
    })
};

exports.resendVerification = function(req, res) {
    var mailToken;
    if (!req.body.email) {
        return rService.send (res, "INVALID_PARAMETERS", "Not enough parameters")
    }

    User.findByEmail(req.body.email, function(err_find, user) {
        if (err_find) {
            return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user")
        }
        if (!user) {
            return rService.send (res, "USER_NOT_FOUND", "USER_NOT_FOUND")
        }
        mailToken = jwtUtils.generateVerifyMailToken(user.id);
        mailService.sendVerifyMail(req.body.email, user.firstName, user.lastName, mailToken);
        log.info({body: req.body}, 'resendVerification - sent successfully');
        return rService.send (res, "OK", "OK")

    })
};

exports.signUp = function(req, res) {
    var mailToken;
    if (!req.body.email || !req.body.password || !req.body.firstName || !req.body.lastName || !req.body.country) {
        return rService.send (res, "INVALID_PARAMETERS", "Not enough parameters");
    }

    var user = new User(req.body);

    User.findByEmail(req.body.email, function (err_find, user_exists) {
        if(user_exists){
            return rService.send(res, "VALIDATION_ERROR", 'EMAIL_TAKEN');
        } else {
            user.save(function (err) {
                if (err) {
                    if (err.name = "ValidationError") {
                        console.log('error', err.errors);
                        if (err.errors && err.errors.password) {
                            log.info({body: req.body}, 'signUp - password validation error');
                            return rService.send (res, "VALIDATION_ERROR", err.errors.password.message)
                        }
                        if (err.errors && err.errors.email) {
                            log.info({body: req.body}, 'signUp - E-mail already taken');
                            if (err.errors.email.message === 'email.NOT_UNIQUE') {
                                return rService.send(res, "VALIDATION_ERROR", 'EMAIL_TAKEN')
                            }
                        }
                    }

                    // Catchall
                    log.error({body: req.body, err: err}, 'signUp - Other error');
                    return rService.send (res, "ERROR_SAVING_USER", "Error saving user")
                } else {
                    mailToken = jwtUtils.generateVerifyMailToken(user._id);
                    console.log(mailToken, 'mail token');
                    mailService.sendVerifyMail(req.body.email, user.firstName, user.lastName, mailToken);
                    //send notifiction message to slack
                    var user_details = [{
                        text:"name: " + user.firstName + ' ' + user.lastName + "\nemail: " + req.body.email
                    }];
                    slack.sendMessage('New user has been registered.', '#registrations', 'casterybot', user_details);
                    log.info({body: req.body}, 'signUp - signed up successfully');
                    return rService.send (res, "OK", "OK")
                }
            })
        }
    });
};

exports.forgot_pass = function (req, res) {
    if (!req.body.email) {
        return rService.send (res, "INVALID_PARAMETERS", "Not enough parameters");
    }

    User.findOne({email: req.body.email}, findCallback);

    function findCallback(error, user) {
        if (error || !user) {
            return rService.send(res, "ERROR_FINDING_USER", "Error finding user, invalid email address")
        }
        var mailToken = jwtUtils.generateVerifyMailToken(user.id);
        mailService.sendForgotMail(req.body.email, mailToken);
        return rService.send(res, "OK", "OK");
    }
};

exports.recover_pass = function (req, res) {
    if (!req.body.password || !req.body.repeatPassword || !req.body.mailToken) {
        return rService.send (res, "INVALID_PARAMETERS", "Not enough parameters");
    }

    jwtUtils.checkVerifyMailToken (req.body.mailToken, function (err, decoded) {

        console.log(decoded);
        if (err || !decoded) {
            log.warn({token: req.body.mailToken}, "recoverPass: invalid resource token");
            return rService.send(res, "INVALID_MAIL_TOKEN", "INVALID_MAIL_TOKEN");
        }

        User.findById (decoded.id, function (err, user) {
            if (err || !user) {
                return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user");
            }
            user.password = req.body.password;
            user.save(function (err) {
                if (err) {
                    log.error({body: req.body, err: err}, 'verifyMail: User error');
                    return rService.send (res, "ERROR_SAVING_USER", "ERROR_SAVING_USER");
                } else {
                    return rService.send (res, "OK", "OK");
                }
            })
        })

    })
};

exports.updateProfile = function(req, res) {
    if (!req.body.firstName || !req.body.lastName || !req.body.country) {
        return rService.send (res, "INVALID_PARAMETERS", "Not enough parameters");
    }

    User.findOne({ '_id': req.auth.sub }, function (err, user) {
        if (err) {
            return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user")
        }
        if (!user) {
            return rService.send (res, "USER_NOT_FOUND", "User not found")
        } else {
            user.firstName = req.body.firstName;
            user.lastName = req.body.lastName;
            user.country = req.body.country;
            user.save(function (err) {
                if (err) {
                    log.error({body: req.body, err: err}, 'updateProfile - error');
                    return rService.send (res, "ERROR_SAVING_USER", "Error saving user");
                } else {
                    return rService.send (res, "OK", "OK");
                }
            })
        }
    })
};

exports.updatePassword = function(req, res) {
    if (!req.body.oldPassword || !req.body.newPassword) {
        return rService.send (res, "INVALID_PARAMETERS", "Not enough parameters");
    }

    User.findOne({ '_id': req.auth.sub }, function (err, user) {
        if (err) {
            return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user");
        }
        if (!user) {
            return rService.send (res, "USER_NOT_FOUND", "User not found");
        } else {

            validatePassword (user.hashed_password, req.body.oldPassword, function (err, result) {
                if (!err && result) {
                    if (!user.verified) {
                        return rService.send (res, "USER_NOT_VERIFIED", "USER_NOT_VERIFIED");
                    }
                    user.password = req.body.newPassword;
                    user.save(function (err) {
                        if (err) {
                            log.error({body: req.body, err: err}, 'updatePassword - error');
                            return rService.send (res, "ERROR_SAVING_USER", "Error saving user");
                        } else {
                            return rService.send (res, "OK", "OK")
                        }
                    })
                }
                else {
                    return rService.send (res, "INVALID_PASSWORD", "INVALID_PASSWORD");
                }
            })
        }
    })
};

exports.verifyMail = function (req, res) {
    if (!req.body.token) {
        return rService.send (res, "INVALID_PARAMETERS", "Not enough parameters");
    }
    jwtUtils.checkVerifyMailToken (req.body.token, function (err, decoded) {
        if (err || !decoded) {
            log.warn({token: resToken}, "verifyMail: invalid resource token");
            return rService.send(res, "INVALID_MAIL_TOKEN", "INVALID_MAIL_TOKEN")
        }
        User.findById (decoded.id, function (err, user) {
            if (err || !user) {
                return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user");
            }
            user.verified = true;
            user.save(function (err) {
                if (err) {
                    log.error({body: req.body, err: err}, 'verifyMail: User error');
                    return rService.send (res, "ERROR_SAVING_USER", "ERROR_SAVING_USER");
                } else {
                    //send user confirmation message to slack's registration channel
                    var user_details = [{
                        text:"name: " + user.firstName + ' ' + user.lastName + "\nemail: " + user.email
                    }];
                    slack.sendMessage('User has confirmed.', '#registrations', 'casterybot', user_details);
                    return rService.send (res, "OK", "OK")
                }
            })
        })

    })

};

exports.logIn = function(req, res) {
    if (!req.body.email || !req.body.password) {
        return rService.send (res, "INVALID_PARAMETERS", "INVALID_PARAMETERS")
    }
    User.findByEmail(req.body.email, function(err_find, user) {
        if (err_find) {
            return rService.send (res, "ERROR_FINDING_USER", "There was an error finding the user")
        }
        if (!user) {
            return rService.send (res, "USER_NOT_FOUND", "USER_NOT_FOUND")
        }
        validatePassword (user.hashed_password, req.body.password, function (err, result) {
            var token = jwtUtils.generateToken(user.id);
            if (!err && result) {
                if (!user.verified) {
                    return rService.send (res, "USER_NOT_VERIFIED", "USER_NOT_VERIFIED");
                }
                return rService.send (res, "OK", "OK", {token: token})
            }
            else {
                return rService.send (res, "INVALID_PASSWORD", "INVALID_PASSWORD")
            }
        })

    })
};
