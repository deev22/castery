'use strict';
var express = require('express');
var passport = require('passport');
var controller = require('./auth.controller');
var jwtUtils = require ('../../services/jwt-utils');

var router = express.Router();

router.get('/soundcloud', function (req, res) {
    var state = req.query.ut;
    if (!state) {
        // TODO render an error
    }
    else {
        passport.authenticate('soundcloud', {session: false, state: state})(req, res);
    }
});
router.get('/soundcloud/callback', controller.scCallback);
router.get('/failure', function(req, res) { res.render('after-auth', { state: 'failure', token: null }); });

router.get('/me', jwtUtils.jwtCheck, controller.me);
router.post('/me', jwtUtils.jwtCheck, controller.updateProfile);
router.post('/update-password', jwtUtils.jwtCheck, controller.updatePassword);
router.post('/signup', controller.signUp);
router.post('/login', controller.logIn);
router.post('/forgot-pass', controller.forgot_pass);
router.post('/recover-pass', controller.recover_pass);
router.post('/verify/resend', controller.resendVerification);
router.post('/verify', controller.verifyMail);
router.get('/renew-token', jwtUtils.jwtCheckNoCred, controller.renew);

module.exports = router;