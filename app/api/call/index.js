'use strict'

var express = require('express')
var controller = require('./call.controller')
var jwtUtils = require ('../../services/jwt-utils')
var router = express.Router()

router.post('/record-start/', jwtUtils.jwtCheck, controller.recordStart);
router.post('/record-stop/', jwtUtils.jwtCheck, controller.recordStop);

router.post('/generate-token', controller.generateToken);
router.post('/twiml-voice', controller.twimlVoice);


router.post('/status', controller.statusRouteHandler);
router.get('/status', controller.statusRouteHandler);

router.post('/serverListener', controller.serverListener);

module.exports = router;
