var Archive = require('../../models/archive')
  , User = require('../../models/user')
  , rService = require ('../../services/responses')
  , twilioClient = require ('../../services/twilio-client')
  , twilio = require('twilio')
  , async = require('async')
  , bucket = require ('../../services/gcloud-factory').bucket
  , url = require('url')
  , configs = require('../../../config')
  , resumable = require ('../../services/resumable');

clients = [];

var mailService = require ('../../services/verifyMail');
var stringify = require('node-stringify');

exports.recordStop = function (req, res) {

  console.log('recordStop');
  // Mind that we can't rely on this being called, if the client dies prematurely.
  // TODO skylink offline recording stop
  return rService.send(res, "OK", {});

};

exports.recordStart = function (req, res) {

  var fileNames
    , generateUrls
    , newArchive = new Archive();

  console.log('recordStart');

  if (!req.body.guestArray) {
    return rService.send (res, "GUEST_ARRAY_NEEDED", "Guest array needed");
  }

  if (!req.body.name) {
    return rService.send (res, "ARCHIVE_NAME_NEEDED", "Archive name needed");
  }

  try {
    fileNames = req.body.guestArray.map(function(guestObj) {
      return guestObj.fileName;
    })
  }
  catch(e) {
    return rService.send (res, "GUEST_ARRAY_NEEDED", "Invalid guest array");
  }

  // TODO skylink offline recording start

  newArchive.creator = req.auth.sub;
  newArchive.name = req.body.name;
  newArchive.localRecordings = fileNames;

  newArchive.save(function(err, archive) {

    if (err) {
        console.log(err);
      return rService.send (res, "ERROR_STORING_ARCHIVE", "Error storing archive")
    }

    generateUrls = generateUploadUrl(req.auth.sub, newArchive._id, req.get('origin'));

    async.map(req.body.guestArray, generateUrls, function(err, result) {

      if (err) {
        // TODO delete archive?
        return rService.send (res, "ERROR_GENERATING_URLS", "Error generating upload urls")
      }

      return rService.send(res, "OK", {uploadArray: result, archive: archive})

    })

  })

};

exports.generateToken = function (req, res) {
    var clientName = req.body.clientName;
    var is_host = req.body.is_host;
    var token = twilioClient.generateToken(clientName, is_host);
    return rService.send(res, "OK", {token: token});
};

exports.twimlVoice = function (req, res) {
    var hostname = req.headers.host;
    var baseUrl = 'https://' + hostname;


    number = req.body.conferenceName;
    var isHost = req.body.isHost;

    var resp = new twilio.TwimlResponse();

    if(isHost) {
        resp.dial(function (node) {
            node.conference(number, {
                beep: 'onEnter',
                startConferenceOnEnter: 'true',
                muted: "false",
                endConferenceOnExit: "false",
                statusCallback: baseUrl + '/call/status',
                statusCallbackEvent: 'start end join leave mute',
                waitUrl: ''
            });
        });
    } else {
        resp.dial(function (node) {
            node.conference(number, {
                beep: false,
                startConferenceOnEnter: "true",
                muted: "false",
                endConferenceOnExit: "false",
                statusCallback: baseUrl + '/call/status',
                statusCallbackEvent: 'start end join leave mute',
                startConferenceOnEnter: false,
                waitUrl: ''
            });
        });
    }
    res.type('text/xml');
    res.send(resp.toString());
};

exports.statusRouteHandler = function (req, res) {
    var confereceName = req.body.FriendlyName;
    var iosa = io.of('/' + confereceName);
    console.log('STATUS ACTION!!!!!!');
    var eventName = req.body.StatusCallbackEvent;
    var callSid = req.body.CallSid;
    var data = {
        name: req.body.FriendlyName,
        conference: req.body.ConferenceSid,
        ConferenceSid: req.body.ConferenceSid
    };

    var client = twilio(configs.TWILIO_ACCOUNT_SID, configs.TWILIO_AUTH_TOKEN);
    client.calls(callSid).get(function(err, call) {
        if (err) {
	        return res.status(500).send('error!');
        }

        data.clientName = call.fromFormatted;
        data.muted = req.body.Muted === 'true';
        data.sid = callSid;

        if (eventName.indexOf('participant-') === 0) {
            if(eventName === 'participant-join' && data.clientName.indexOf('_Host')>0) {
	            returnres.status(200).send('OK');
            } else {
                iosa.emit(eventName, JSON.stringify(data));
	            return res.status(200).send('OK');
            }
        }
    });
};


exports.serverListener = function (req, res) {
    console.log('server Listener ACTION!!!!!!');
    var confereceName = req.body.conferenceName;
    console.log(confereceName);

    if(namespaces[confereceName]) {
        console.log('FINDEX NMAESPACE,RETURN');
        res.status(200).send('OK');
        return;
    }

    var iosa = io.of('/' + confereceName);
    namespaces[confereceName] = iosa;
    
    iosa.on('connection', function(socket){
        console.log('Connected to Conference namespace_ ' + confereceName);
        //guest joined
        socket.on('join', function (msg) {
           console.log('JOIN EVENT FAIRED');
           console.log('USER IS ' + msg);
            var client = {
                socket_id: socket.id,
                username: msg.message.username,
                user_id: msg.message.user_id
            };
            clients.push(client);
            console.log(clients);

        });

        socket.on('START_RECORDING', function(msg){
            console.log('START_RECORDING');
            iosa.emit('START_RECORDING', msg.message);
        });


        socket.on('STOP_RECORDING', function(msg){
            console.log('STOP_RECORDING',msg);
            iosa.emit('STOP_RECORDING', '');
        });

        //guest send a message
        socket.on('message', function(msg){
            console.log('RECEIVED DATA FROM CLIENT ' + msg);
            iosa.emit('connectionsChanged', msg.message);
        });
    });
	return res.status(200).send('OK');
};


function generateUploadUrl(sub, archiveID, origin) {

  var creator = sub
    , id = archiveID;

  return function generateUploadUrl(fileObj, cb) {

    var file = bucket.file(creator + '/recordings/' + id + '/' + fileObj.fileName);

    resumable.createURI({
      authClient: file.bucket.storage.authClient,
      bucket: file.bucket.name,
      file: file.name,
      generation: file.generation,
      metadata: {},
      origin: origin
    }, function(err, url) {
      if (err) {
        cb(err, null);
        return
      }

      cb(null, {
        url: url,
        id: fileObj.id
      })
    })

  }

}
