'use strict';

var express = require('express'),
	controller = require('./sound.controller'),
	router = express.Router(),
	jwtUtils = require ('../../services/jwt-utils');

router.post('/', jwtUtils.jwtCheck, controller.getUploadUrl);
router.post('/finalize', jwtUtils.jwtCheck, controller.finalizeUpload);
/*router.get('/:soundId', controller.getDownloadUrl)*/
router.get('/download', jwtUtils.jwtCheck, controller.getDownloadAll);
router.get('/', jwtUtils.jwtCheck, controller.getSoundList);
router.delete('/:soundId', jwtUtils.jwtCheck, controller.deleteSound);

module.exports = router;