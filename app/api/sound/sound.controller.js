"use strict";

var Sound = require('../../models/sound')
  , rService = require('../../services/responses')
  , async = require('async')
  , bucket = require ('../../services/gcloud-factory').bucket;

exports.getUploadUrl = function(req, res) {

    var file;

    if (!req.body.name) {
        return rService.send (res, "INVALID_PARAMETERS", "Name needed");
    }

    file = bucket.file(req.auth.sub + '/sounds/' + req.body.name);

    file.getSignedUrl({
        action: 'write',
        expires: Math.round(Date.now()) + (60 * 60 * 2 * 1000) // 2 hours.
    }, function(err, url) {
        if (err) return rService.send(res, 'STORAGE_ERROR', 'Error generating upload url');
        return rService.send(res, 'OK', {uploadUrl: url})
    })
};

exports.finalizeUpload = function(req, res) {
    var sound;
    if (!req.body.name) {
        return rService.send (res, "INVALID_PARAMETERS", "Name needed")
    }
    sound = new Sound({
        name: req.body.name
      , creator: req.auth.sub
    });

    sound.save(function (error, soundObj) {
        if (error) {
            if (error.name === 'ValidationError') return rService.send(res, "ERROR_SOUND_NOT_UNIQUE", "Sound name must be unique");
            else return rService.send(res, "ERROR_SAVING_SOUND", "Error saving sound")
        }
        if (!soundObj) {
            return rService.send(res, "ERROR_SAVING_SOUND", "Error saving sound")
        }
        return rService.send(res, "OK", {soundObj: soundObj})
    })
};

exports.getSoundList = function(req, res) {
    Sound.find({ creator: req.auth.sub }, findCallback);
    function findCallback(error, sounds) {
        if (error) {
            return rService.send(res, "ERROR_FINDING_SOUND", "Error finding sound")
        }
        return rService.send(res, "OK", sounds)
    }
};

exports.getDownloadAll = function(req, res) {
    Sound.find({ creator: req.auth.sub }, findCallback);
    function findCallback(error, sounds) {
        if (error) {
            return rService.send(res, "ERROR_FINDING_SOUND", "Error finding sound")
        }
        async.map(sounds, createDownloadURL, function(err, results){
            return rService.send(res, "OK", results)
        })
    }
};

exports.deleteSound = function(req, res) {
    var sound;
    if (!req.params.soundId) {
        return rService.send (res, "INVALID_PARAMETERS", "Id needed");
    }
    Sound.findById(req.params.soundId, findCallback);
    function findCallback(error, sound) {
        if (error) {
            return rService.send(res, "ERROR_FINDING_SOUND", "Error finding sound")
        }

        sound.remove(function (err, snd) {
            if (err) {
                return rService.send (res, "ERROR_REMOVING_SOUND", "Error removing sound");
            }
            else {
                return rService.send (res, "OK", "OK");
            }
        });
        deleteFile(req.auth.sub, req.body.name)
    }
};

function createDownloadURL(fileObj, cb) {
    var file = bucket.file(fileObj.creator + '/sounds/' + fileObj.name);
    file.getSignedUrl({
        action: 'read',
        expires: Math.round(Date.now()) + (100 * 60 * 60 * 1000) // 100 hrs
    }, function(err, url) {
        if (!err) cb(null, { name: fileObj.name, url: url, id: fileObj.id });
        else {
            console.error(err);
            cb(null, { name: fileObj.name, url: null, id: fileObj.id })
        }
    })
}

function deleteFile(sub, name) {
    var file = bucket.file(sub + '/sound/' + name);
    file.delete(function(err, apiResponse) {
        if (err) {
            console.error('TODO log - Error removing file: ' + name);
        }
        else {
            console.log('TODO log - Removed file: ' + name)
        }
    })
}
