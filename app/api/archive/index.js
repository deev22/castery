'use strict';

var express = require('express');
var controller = require('./archive.controller');
var router = express.Router();
var jwtUtils = require ('../../services/jwt-utils');

router.get('/', jwtUtils.jwtCheck, controller.getArchives);
router.get('/:id', jwtUtils.jwtCheck, controller.getArchive);
router.delete('/:id', jwtUtils.jwtCheck, controller.deleteArchive);

module.exports = router;
