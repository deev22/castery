"use strict";

var Archive = require('../../models/archive')
  , rService = require('../../services/responses')
  , bucket = require('../../services/gcloud-factory').bucket
  , async = require('async');

exports.getArchives = function (req, res) {

  console.log('getArchives called');

  Archive.find({ creator: req.auth.sub }, findCallback);
  function findCallback(error, archives) {
    if (error) {
      return rService.send(res, "ERROR_FINDING_ARCHIVE", "Error finding archives")
    }
    return rService.send (res, "OK", archives);
  }
};

exports.getArchive = function (req, res) {
  console.log ("GET ARCHIVE ID " + req.params.id);
  if (!req.params.id) {
    return rService.send (res, "ARCHIVE_ID_NEEDED", "Archive id needed")
  }


  Archive.findOne({ creator: req.auth.sub, _id: req.params.id }, findCallback);


  function findCallback(error, archive) {

    if (error) {
      return rService.send(res, "ERROR_FINDING_ARCHIVE", "Error finding archive")
    }

    bucket.getFiles({
      prefix: req.auth.sub + '/recordings/' + req.params.id + '/'
    }, function(err, files) {
      if (err) {
        return rService.send(res, "ERROR_FINDING_ARCHIVE", "Error finding archive files")
      }

      async.map(files, assignSignedUrl, function(err, fileUrls) {
        if (err) {
          return rService.send(res, "ERROR_FINDING_ARCHIVE", "Error finding archive signed URLS")
        }

        return rService.send(res, "OK", { files: files, urls: fileUrls })

      })

    })
  }
};

exports.deleteArchive = function (req, res) {
  console.log ("DELETE ARCHIVE ID " + req.params.id);
  if (!req.params.id) {
    return rService.send (res, "ARCHIVE_ID_NEEDED", "Archive id needed");
  }
  Archive.findOne({ creator: req.auth.sub, _id: req.params.id }, findCallback);
  function findCallback(error, archive) {
    if (error  || !archive) {
      return rService.send(res, "ERROR_FINDING_ARCHIVE", "Error finding archive");
    }
    archive.remove(function (err, arch) {
      if (err || !arch) {
        return rService.send (res, "ERROR_REMOVING_ARCHIVE", "Error removing archive");
      }
      else {
        bucket.deleteFiles({
          force: true,
          prefix: req.auth.sub + '/recordings/' + req.params.id + '/'
        }, function(err) {
          if (!err) {
            console.log('deleted directory: ' +  req.auth.sub + '/recordings/' + req.params.id + '/')
          }
          else {
            console.error('error deleting: ' +  req.auth.sub + '/recordings/' + req.params.id + '/');
            console.error(err);
          }
        });
        return rService.send (res, "OK", "OK");
      }
    })
  }
};

