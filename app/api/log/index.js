'use strict';

var express = require('express');
var controller = require('./log.controller');
var router = express.Router();

router.post('/call/:callID', controller.logCall);

module.exports = router;