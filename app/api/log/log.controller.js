"use strict"

var rService = require ('../../services/responses')
var mandrill = require('mandrill-api/mandrill')
var mandrill_client = new mandrill.Mandrill('<Your-mandrill-key>')

exports.logCall = function (req, res) {

    var callID = req.params.callID
      , message

    console.log("Log called from " + callID)

    if (!callID) return rService.send(res, "OK", "OK")

     message = {
        "html": '<pre>' + req.text + '</pre>',
        "subject": "Castery - Test mail " + callID,
        "from_email": "test-noreply@castery.com",
        "from_name": "Castery Test",
        "to": [
            {
                "email": "janesconference@gmail.com",
                "name": "Cristiano",
                "type": "to"
            },
            {
                "email": "Jeff@jeffwaters.com",
                "name": "Jeff",
                "type": "to"
            }
        ],
        "headers": {
            "Reply-To": "test-noreply@castery.com"
        },
        "important": false,
        "track_opens": null,
        "track_clicks": null,
        "auto_text": null,
        "auto_html": null,
        "inline_css": null,
        "url_strip_qs": null,
        "preserve_recipients": null,
        "view_content_link": null,
        "bcc_address": null,
        "tracking_domain": null,
        "signing_domain": null,
        "return_path_domain": null,
        "tags": [
            "test"
        ]
    };
    var async = false;
    mandrill_client.messages.send({"message": message, "async": async}, function(result) {
        console.log(result);
    }, function(e) {
        // Mandrill returns the error as an object with name and message keys
        console.error('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    });

    return rService.send(res, "OK", "OK")

}
